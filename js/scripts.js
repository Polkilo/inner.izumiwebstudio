$(window).resize(function(event) {
	//event.stopImmediatePropagation();
	header_fixed_class();
	$('.about-slide').slick('setPosition');
	$('.slide').slick('setPosition');
	$('.team-slide').slick('setPosition');
});

$(document).ready(function(){
	header_fixed_class();
	photo();

	$('.bg').each(function() {
		if ($(this).find('> img').length) {
			$(this).css('background-image', 'url(' + $(this).find('> img').attr('src') + ')');
		}
	});

	$('.about-slide').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		dots: true,
		infinite: false,
		autoplay: true,
		autoplaySpeed: 9000,
		fade: true,
		cssEase: 'linear',
	});
	$('.about-slide').on('beforeChange', function(event, slick, currentSlide){
		$('.slick-slide .typed').removeClass('start');
		$('.slick-slide .typed').removeClass('done');
		$('.slick-slide.slick-active .typed').addClass('start');
		typedJs();
	});
	$('.about-slide').on('afterChange', function(event, slick, currentSlide){
		$('.slick-slide .typed').removeClass('done');
		$('.slick-slide .typed').removeClass('start');
	});
	typedJs();

	$('.slide').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		dots: true,
		centerMode: true,
		centerPadding: '24%',
		lazyLoad: 'ondemand',
		autoplay: true,
		autoplaySpeed: 10000,
		variableWidth: false,
		responsive: [
			{
				breakpoint: 1045,
				settings: {
					centerPadding: '12%',
					variableWidth: false,
				}
			}
		]
	});

	$('.team-slide').slick({
		slidesToShow: 5,
		slidesToScroll: 1,
		arrows: false,
		dots: true,
		autoplay: true,
		autoplaySpeed: 4000,
		cssEase: 'linear',
		responsive: [
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
				}
			},
			{
				breakpoint: 500,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
				}
			}
		]
	});

	$('<span class="open-menu"><span></span><span></span><span></span><span></span></span>').appendTo('#header > .container');
	$('html').on('click', '.open-menu', function() {
		$('body').toggleClass('menu-opened');
		$('.about-slide').slick('setPosition');
		$('.slide').slick('setPosition');
		$('.team-slide').slick('setPosition');
		setTimeout(function () {
			$('.fader .fader_logo').toggleClass('animated');
		}, 100);
		return false;
	});

	$('.outer-nav ul li a, .btn-primary').on('click', function(e) {
		var href = $(this).attr("href");
		$('body').removeClass('menu-opened');
		var offsetTop = href === "#" ? 0 : $(href).offset().top - $('#header').outerHeight() + 1;
		$('html, body').stop().animate({
			scrollTop: offsetTop
		}, 500);
		setTimeout(function () {
			$('.fader .fader_logo').removeClass('animated');
		}, 100);
		$('.about-slide').slick('setPosition');
		$('.slide').slick('setPosition');
		$('.team-slide').slick('setPosition');
		e.preventDefault();
	});


	$('.fader').on('click', function() {
		$('body').removeClass('menu-opened');
		$('.about-slide').slick('setPosition');
		$('.slide').slick('setPosition');
		$('.team-slide').slick('setPosition');
		setTimeout(function () {
			$('.fader .fader_logo').removeClass('animated');
		}, 100);
	}).on('touchmove', function() {
		if ($('body').hasClass('menu-opened') && $(window).width() < 992) {
			$('body').removeClass('menu-opened');
			setTimeout(function () {
				$('.fader .fader_logo').removeClass('animated');
			}, 100);
		}
	});

	$(".fancybox").fancybox({
		openEffect: 'elastic',
		closeEffect: 'elastic',
		openSpeed: 300,
		closeSpeed: 300,
		beforeShow: function(){
			$('.btn_pop').on('click', function(e) {
				e.preventDefault();
				$('#popup2').addClass('visible');
				$('.close').click(function(e) {
					e.preventDefault();
					$('#popup2').removeClass('visible');
				});
			});
		},
		afterClose: function(){
			$('#popup2').removeClass('visible');
		},
		onInit: bindPrivacyPolicyAnchors(),
	});

});

function photo(){
	$('.group-list li').click(function() {
		var Img = $('.img-group .first').attr('src');
		var thisImg = $(this).find('> img').attr('src');

		$('.img-group .first').removeClass('animate');
		setTimeout(function () {
			$('.img-group .first').addClass('animate').attr('src', thisImg);
		}, 390);
		//$('.img-group .first').addClass('animate').attr('src', thisImg);
		//$(this).find('> img').attr('src', Img);
	});
}

function bindPrivacyPolicyAnchors(){
	$('.ancorn-list li a').on('click', function(e) {
		var _href = $(this).attr("href");
		var _offsetTop = _href === "#" ? 0 : document.querySelector(_href).offsetTop;
		$('body .fancybox-inner').stop().animate({
			scrollTop: _offsetTop
		}, 400);
		e.preventDefault();
	});
}

function typedJs () {
	$('.typed').each(function() {
		var textEl = $(this).find('.typed-js').attr('data-text'),
			_this = $(this),
			_thisEl = $(this).find('.typed-js');
		if ( $(this).is(':in-viewport') && !$(this).hasClass('start')) {
			setTimeout(function() {
				_this.addClass('start');
				_thisEl.typed({
					strings: [textEl],
					typeSpeed: 100,
					preStringTyped: function() {
						$('.typed-cursor').css('display', 'inline-block');
					},
					callback: function() {
						_thisEl.parent().addClass('done');
						$('.typed-cursor').fadeTo("slow", 0);
					}
				});
			}, 500)
		}
	});
}


function header_fixed_class(){
	var heightEl  = $('.visual').outerHeight() - $('#header').outerHeight();
	$(window).scrollTop() > heightEl ? $('#header').addClass('modify') : $('#header').removeClass('modify');
	$(window).scrollTop() > heightEl ? $('#wrapper').addClass('header_fixed') : $('#wrapper').removeClass('header_fixed');
	$(window).scroll(function(){
		event.stopImmediatePropagation();
		$(window).scrollTop() > heightEl ? $('#header').addClass('modify') : $('#header').removeClass('modify');
		$(window).scrollTop() > heightEl ? $('#wrapper').addClass('header_fixed') : $('#wrapper').removeClass('header_fixed');
	});
}

function svg(){
	new Vivus('logo_svg', {
		type: 'oneByOne',
		duration: 350,
		animTimingFunction: Vivus.EASE,
		delay: 349,
		dashGap: 0,
	}, function (obj) {
		obj.el.classList.add('finished');
	});
}svg();

(function() {
	if (!String.prototype.trim) {
		(function() {
			// Make sure we trim BOM and NBSP
			var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
			String.prototype.trim = function() {
				return this.replace(rtrim, '');
			};
		})();
	}
	[].slice.call( document.querySelectorAll( '.form-control' ) ).forEach( function( inputEl ) {
		// in case the input is already filled..
		if( inputEl.value.trim() !== '' ) {
			classie.add( inputEl.parentNode, 'input--filled' );
		}
		// events:
		inputEl.addEventListener( 'focus', onInputFocus );
		inputEl.addEventListener( 'blur', onInputBlur );
	} );
	function onInputFocus( ev ) {
		classie.add( ev.target.parentNode, 'input--filled' );
	}
	function onInputBlur( ev ) {
		if( ev.target.value.trim() === '' ) {
			classie.remove( ev.target.parentNode, 'input--filled' );
		}
	}
})();