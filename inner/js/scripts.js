$(window).resize(function(event) {
	event.stopImmediatePropagation();
	header_fixed_class();
	$('.about-slide').slick('setPosition');
	$('.slide').slick('setPosition');
});

$(document).ready(function(){

	header_fixed_class();

	$('.bg').each(function() {
		if ($(this).find('> img').length) {
			$(this).css('background-image', 'url(' + $(this).find('> img').attr('src') + ')');
		}
	});

	$('.about-slide').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		dots: true,
		infinite: false,
		autoplay: true,
		autoplaySpeed: 9000,
		fade: true,
		cssEase: 'linear',
	});
	$('.about-slide').on('beforeChange', function(event, slick, currentSlide){
		$('.slick-slide .typed').removeClass('start');
		$('.slick-slide .typed').removeClass('done');
		$('.slick-slide.slick-active .typed').addClass('start');
		typedJs();
	});
	$('.about-slide').on('afterChange', function(event, slick, currentSlide){
		$('.slick-slide .typed').removeClass('done');
		$('.slick-slide .typed').removeClass('start');
	});
	typedJs();

	$('.slide').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		dots: true,
		centerMode: true,
		centerPadding: '24%',
		autoplay: true,
		autoplaySpeed: 10000,
		variableWidth: false,
		responsive: [
			{
			breakpoint: 1045,
				settings: {
					centerPadding: '12%',
					variableWidth: false,
				}
			}
		]
	});

	$('.outer-nav ul li a, .btn-primary').on('click', function(e) {
		var href = $(this).attr("href");
		$('body').removeClass('menu-opened');
		var offsetTop = href === "#" ? 0 : $(href).offset().top - $('#header').outerHeight() + 5;
		$('html, body').stop().animate({
			scrollTop: offsetTop
		}, 500);
		
		e.preventDefault();
	});

	scrambles();

	$(".fancybox").fancybox({
		openEffect: 'elastic',
		closeEffect: 'elastic',
		openSpeed: 300,
		closeSpeed: 300,
		beforeShow: function(){
			$('.btn_pop').on('click', function(e) {
				e.preventDefault();
				$('#popup2').addClass('visible');
				$('.close').click(function(e) {
					e.preventDefault();
					$('#popup2').removeClass('visible');
				});
			});
		},
		afterClose: function(){
			$('#popup2').removeClass('visible');
		},
		onInit: bindPrivacyPolicyAnchors(),
	});

});

function bindPrivacyPolicyAnchors(){
	$('.ancorn-list li a').on('click', function(e) {
		var _href = $(this).attr("href");
		var _offsetTop = _href === "#" ? 0 : document.querySelector(_href).offsetTop;
		$('body .fancybox-inner').stop().animate({
			scrollTop: _offsetTop
		}, 400);
		e.preventDefault();
	});
}


function typedJs () {
	$('.typed').each(function() {
		var textEl = $(this).find('.typed-js').attr('data-text'),
			_this = $(this),
			_thisEl = $(this).find('.typed-js');
		if ( $(this).is(':in-viewport') && !$(this).hasClass('start')) {
			setTimeout(function() {
				_this.addClass('start');
				_thisEl.typed({
					strings: [textEl],
					typeSpeed: 100,
					preStringTyped: function() {
						$('.typed-cursor').css('display', 'inline-block');
					},
					callback: function() {
						_thisEl.parent().addClass('done');
						$('.typed-cursor').fadeTo("slow", 0);
					}
				});
			}, 500)
		}
	});
}


function scrambles() {

	var r_currentTop = $(window).scrollTop();
	var r_elems = $('.content > section');
	var r_massNavItems = $('#header .outer-nav li');
	r_elems.each(function(index){
		var r_elemTop = $(this).offset().top - $('#header').outerHeight();
		var r_elemBottom = r_elemTop + $(this).outerHeight();
		var r_id = $(this).attr('id');
		var r_navElem = $('a[href="#' + r_id+ '"]');
		if((r_currentTop >= r_elemTop) && (r_currentTop <= r_elemBottom)){
			for (var i = 0; i < $(this).attr('scramble-nav'); i++) {
				if (!r_massNavItems[i].classList.contains('active')) {
					r_massNavItems[i].classList.add('active');
					r_massNavItems[i].classList.add('active_el');
				}
			}
		} else if (r_currentTop <= r_elemTop) {
			r_navElem.parent().removeClass('active');
			r_navElem.parent().removeClass('active_el');
		}
	})


	$(window).bind('scroll', function(event) {
		var currentTop = $(window).scrollTop();
		var elems = $('.content > section');
		var _massNavItems = $('#header .outer-nav li');
		elems.each(function(index){
			var elemTop = $(this).offset().top - $('#header').outerHeight();
			var elemBottom = elemTop + $(this).outerHeight();
			var id = $(this).attr('id');
			var navElem = $('a[href="#' + id+ '"]');
			if((currentTop >= elemTop) && (currentTop <= elemBottom)){
				navElem.parent().addClass('active_el');
				navElem.parent().prev().addClass('active_del');
				for (var i = 0; i < $(this).attr('scramble-nav'); i++) {
					if (!_massNavItems[i].classList.contains('active')) {
						_massNavItems[i].classList.add('active');
					}
				}
			} else if (currentTop <= elemTop) {
				navElem.parent().removeClass('active');
				navElem.parent().removeClass('active_el');
				navElem.parent().removeClass('active_del');
			} else {
				navElem.parent().removeClass('active_del');
				navElem.parent().removeClass('active_el');
			}
		})
	});

}

function header_fixed_class(){
	var heightEl  = $('.visual').outerHeight() - $('#header').outerHeight();
	$(window).scrollTop() > heightEl ? $('#header').addClass('modify') : $('#header').removeClass('modify');
	$(window).scrollTop() > heightEl ? $('#wrapper').addClass('header_fixed') : $('#wrapper').removeClass('header_fixed');
	$(window).scroll(function(){
		event.stopImmediatePropagation();
		$(window).scrollTop() > heightEl ? $('#header').addClass('modify') : $('#header').removeClass('modify');
		$(window).scrollTop() > heightEl ? $('#wrapper').addClass('header_fixed') : $('#wrapper').removeClass('header_fixed');
	});
}

function svg(){
	new Vivus('logo_svg', {
		type: 'oneByOne',
		duration: 350,
		animTimingFunction: Vivus.EASE,
		delay: 349,
		dashGap: 0,
	}, function (obj) {
		obj.el.classList.add('finished');
	});
}svg();

(function() {
	if (!String.prototype.trim) {
		(function() {
			// Make sure we trim BOM and NBSP
			var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
			String.prototype.trim = function() {
				return this.replace(rtrim, '');
			};
		})();
	}
	[].slice.call( document.querySelectorAll( '.form-control' ) ).forEach( function( inputEl ) {
		// in case the input is already filled..
		if( inputEl.value.trim() !== '' ) {
			classie.add( inputEl.parentNode, 'input--filled' );
		}
		// events:
		inputEl.addEventListener( 'focus', onInputFocus );
		inputEl.addEventListener( 'blur', onInputBlur );
	} );
	function onInputFocus( ev ) {
		classie.add( ev.target.parentNode, 'input--filled' );
	}
	function onInputBlur( ev ) {
		if( ev.target.value.trim() === '' ) {
			classie.remove( ev.target.parentNode, 'input--filled' );
		}
	}
})();